package com.github.stepancheg.nettytd;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * @author Stepan Koltsov
 */
public class Perftest {

    public static void main(String[] args) throws Exception {
        System.out.println("Stage 1");
        int amount =1000000;
        for (int i=0;i<amount;i++){
            TestCache.getInstance().addRange(MsisdnGanerator.generateNew());
//            System.out.println(i);
        }
        System.out.println("Stage 2");
        Server server = new Server();

        final Semaphore semaphore = new Semaphore(100000);


        int iter = 1000;
        long total = 0;
//        client
        Client client  = new Client(server.getPort(), new Consumer<Long>() {
                @Override
                public void accept(Long integer) {
                    semaphore.release();
                }
            },new ISender() {
            @Override
            public void send0() {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        ClientInstance.getInstance().setClient(client);
//        trx0
        System.out.println("Stage 3");
        ClientSender[] r = new ClientSender[iter];
        for (int i=0;i<iter;i++){
            r[i]=new ClientSender(MsisdnGanerator.generateNew().getMsisdnL());
//            service.submit(new ClientSender(MsisdnGanerator.generateNew().getMsisdnL()));
        }
//        trx1
        ExecutorService service= Executors.newFixedThreadPool(iter);
        System.out.println("Stage 4");
        while(true) {
//            long start = System.currentTimeMillis();

//            while (System.currentTimeMillis() - start < 1000) {
                for (int i = 0; i < iter; ++i) {
                    r[i].run();
//                    semaphore.acquire();
//                    client.send(MsisdnGanerator.generateNew().getMsisdnL());
                }
//                total += iter;
//                iter *= 2;
//            }

//            long dms = System.currentTimeMillis() - start;
//            long rps = total * 1000 / dms;
//            System.out.println("rps: " + rps);
        }
    }
}