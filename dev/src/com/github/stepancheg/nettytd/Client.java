package com.github.stepancheg.nettytd;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author Stepan Koltsov
 */
public class Client {

    private final Channel channel;
    private ISender iSender;

    public Client(int port, final Consumer<Long> consumer, ISender iSender) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(new NioEventLoopGroup());
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                ch.pipeline().addLast(
                        new LongDecoder(),
                        new LongEncoder(),
                        new ChannelInboundHandlerAdapter() {
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                consumer.accept((Long) msg);
                            }
                        }
                );
            }
        });
        ChannelFuture future = bootstrap.connect("localhost", port);
        future.awaitUninterruptibly();
        if (!future.isSuccess()) {
            throw new RuntimeException(future.cause());
        }
        channel = future.channel();
        this.iSender = iSender;
    }

    public void send(long value) {
        iSender.send0();
        long time = System.nanoTime();
        ChannelUtil.writeAndFlush(channel, value);
        time = System.nanoTime()-time;
        ExtraCounter.getInstance().increment(time);
    }
}