package com.github.stepancheg.nettytd;



/**
 * @author: Alexander Zykov
 */
public class TestCache {
    private static final TestCache instance = new TestCache();
    private TestCacheContainer cache = new TestCacheContainer();

    private TestCache() {
    }

    public void addRange(Msisdn msisdn){
        cache.addRange(new Range(msisdn.getMsisdnL(),msisdn.getMsisdnL(),msisdn.getPrefixes()[5],
                msisdn.getPrefixes()[6].toString()));
    }

    public static TestCache getInstance() {
        return instance;
    }

    public Range getRange(Long msisdn){
        return cache.getRange(msisdn);
    }

    public int getSize(){
        return cache.getSize();
    }

}
