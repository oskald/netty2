package com.github.stepancheg.nettytd;

/**
 * @author: Alexander Zykov
 */
public class TestStarter {
    public static void main(String[] args){
        System.out.println("Stage 1");
        int amount =1000000;
        for (int i=0;i<amount;i++){
            TestCache.getInstance().addRange(MsisdnGanerator.generateNew());
//            System.out.println(i);
        }
        System.out.println("Stage 2");
        long generateTime = System.nanoTime();
        for (int i=0;i<amount;i++){
            MsisdnGanerator.generateNew();
        }
        generateTime=System.nanoTime()-generateTime;
        System.out.println(generateTime);
        System.out.println("Stage3");
        long allTime=System.nanoTime();
        for (int i=0;i<amount;i++){
            Msisdn msidn = MsisdnGanerator.generateNew();

            TestCache.getInstance().getRange(msidn.getMsisdnL());
        }
        allTime=System.nanoTime()-allTime;
        System.out.println("Result");
        System.out.println(allTime);
        System.out.println(allTime-generateTime);
        System.out.println(TestCache.getInstance().getSize());
        System.out.println((allTime)/amount);
        System.out.println((allTime-generateTime)/amount);

    }
}
