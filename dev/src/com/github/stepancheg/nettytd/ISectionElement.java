package com.github.stepancheg.nettytd;

/**
 * @author: Alexander Zykov
 */
public interface ISectionElement<R> {
    public ISectionElement getElement(R req);
}
