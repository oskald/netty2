package com.github.stepancheg.nettytd;

/**
 * @author: Alexander Zykov
 */
public class Range implements Comparable<Range>{
    private Long rabgeBegin;
    private Long rangeEnd;
    private Long regionId;
    private String operatorName;
    private Long prefix;
    private int prefixWeight;

    public Range(Long rabgeBegin, Long rangeEnd, Long regionId, String operatorName) {
        this.rabgeBegin = rabgeBegin;
        this.rangeEnd = rangeEnd;
        this.regionId = regionId;
        this.operatorName = operatorName;
        Msisdn msisdnBegin = new Msisdn(rabgeBegin.toString());
        Msisdn msisdnEnd = new Msisdn(rangeEnd.toString());
        int i=0;
        prefix = null;
//        prefixWeight = 0;
//        while (msisdnBegin.getPrefixes()[i]==msisdnEnd.getPrefixes()[i]){
//            prefix = msisdnBegin.getPrefixes()[i];
//            prefixWeight = i+1;
//        }
    }

    public int getPrefixWeight() {
        return prefixWeight;
    }

    public Long getPrefix() {
        return prefix;
    }

    public Long getRabgeBegin() {
        return rabgeBegin;
    }

    public Long getRangeEnd() {
        return rangeEnd;
    }

    public Long getRegionId() {
        return regionId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    @Override
    public int compareTo(Range o) {
        return getRabgeBegin().compareTo(o.getRabgeBegin());
    }
}

