package com.github.stepancheg.nettytd;

/**
 * @author: Alexander Zykov
 */
public class ClientSender implements Runnable{
    private long value;

    public ClientSender(long value) {
        this.value = value;
    }

    @Override
    public void run() {
        ClientInstance.getInstance().getClient().send(value);
    }
}
