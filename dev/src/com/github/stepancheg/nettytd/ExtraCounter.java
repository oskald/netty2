package com.github.stepancheg.nettytd;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: Alexander Zykov
 */
public class ExtraCounter {
    private static final ExtraCounter instance = new ExtraCounter();
    private ReentrantLock lock = new ReentrantLock();
    private long amount = 0;
    private long count = 0;

    private ExtraCounter() {
    }

    public static ExtraCounter getInstance() {
        return instance;
    }
    public void increment(long value){
        try{
            lock.lock();
            amount+=value;
            count++;
        }finally {
            lock.unlock();
        }
    }

    public long[] getAmount() {
        long result0 = 0;
        long result1 = 0;
        try{
            lock.lock();
            result0 =amount;
            result1 =count;
            amount=0;
            count=0;
        }finally {
            lock.unlock();
        }
        return new long[]{result0,result1};
    }
}
