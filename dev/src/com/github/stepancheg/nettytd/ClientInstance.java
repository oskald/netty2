package com.github.stepancheg.nettytd;

/**
 * @author: Alexander Zykov
 */
public class ClientInstance {
    private static final ClientInstance instance = new ClientInstance();
    private Client client = null;

    private ClientInstance() {
    }

    public static ClientInstance getInstance() {
        return instance;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
