package com.github.stepancheg.nettytd;

/**
 * @author: Alexander Zykov
 */
public class WrappedCounter {
    private static final WrappedCounter instance = new WrappedCounter();
    private long startTime=0;
//    private long count=0;

    private WrappedCounter() {
    }

    public static WrappedCounter getInstance() {
        return instance;
    }

    public synchronized void show(){
        if(System.currentTimeMillis()-startTime>1000){
            long[] result = ExtraCounter.getInstance().getAmount();
            startTime=System.currentTimeMillis();
            System.out.println("msg/s: "+result[1]);
            if(result[1]!=0){
                System.out.println("delay: "+(result[0]/result[1]));
            }

//            count=0;
        }
//        count++;
    }
}
