package com.github.stepancheg.nettytd;

/**
 * @author: Alexander Zykov
 */
public class MsisdnGanerator {
    private static final Long Min = 79100000000L;
    private static final Long Max = 79999999999L;
    public static Msisdn generateNew(){

        Long msidnL = Long.valueOf(Min + (long)(Math.random() * ((Max - Min) + 1)));
        return new Msisdn(msidnL.toString());
    }
}
