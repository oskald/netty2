package com.github.stepancheg.nettytd;

/**
 * @author Viktor Kudryavtsev  31.01.14 10:19
 */
public class ShowValue {
    private long value;
    private boolean show;

    public ShowValue(long value, boolean show) {
        this.value = value;
        this.show = show;
    }

    public long getValue() {
        return value;
    }

    public boolean isShow() {
        return show;
    }
}
