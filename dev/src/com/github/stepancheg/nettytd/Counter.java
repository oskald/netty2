package com.github.stepancheg.nettytd;

/**
 * @author Viktor Kudryavtsev  31.01.14 10:17
 */
public class Counter {
    private long count=0;
    private long start=0;
    private static final Counter instance = new Counter();

    public static Counter getInstance() {
        return instance;
    }

    private Counter() {
    }

    public synchronized ShowValue getShowValue() {
        ShowValue result = null;
        if(System.currentTimeMillis()-start>1000){
            start=System.currentTimeMillis();
            result = new ShowValue(count,true);
            count=0;
        }else{
            result = new ShowValue(count,false);
        }
        count++;
        return result;
    }
}
