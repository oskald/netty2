package com.github.stepancheg.nettytd;


import java.util.Map;
import java.util.TreeMap;

/**
 * @author: Alexander Zykov
 */
public class TestCacheContainer {
    private Map<Long,Range> map = new TreeMap<Long, Range>();
    public void addRange(Range range){
        map.put(range.getRabgeBegin(),range);
    }
    public Range getRange(Long msisdn){
        return map.get(msisdn);
    }
    public int  getSize(){
        return map.size();
    }
}
