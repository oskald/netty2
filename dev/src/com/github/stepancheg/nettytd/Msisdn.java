package com.github.stepancheg.nettytd;

/**
 * @author: Alexander Zykov
 */
public class Msisdn {
    private Long msisdnL;
    private String msisdnS;
    private Long[] prefixes=new Long[11];

    public Msisdn(String msisdnS) {
        this.msisdnS = msisdnS;
        msisdnL = Long.valueOf(msisdnS);
        initPrefixes();
    }
    private void initPrefixes(){
        for (int i=0;i<11;i++){
            prefixes[i]=Long.valueOf(msisdnS.substring(0,i+1));
        }
    }

    public Long getMsisdnL() {
        return msisdnL;
    }

    public String getMsisdnS() {
        return msisdnS;
    }

    public Long[] getPrefixes() {
        return prefixes;
    }
}
