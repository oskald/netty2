package ru.intervale.natty.training;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: Rasta-Laptop
 * Date: 04.11.12
 * Time: 14:16
 */
public class NettyServer extends SimpleChannelHandler implements Runnable {
    private static ChannelGroup cg = new DefaultChannelGroup();
    private static BlockingQueue<Channel> bq = new LinkedBlockingQueue<Channel>();
    static volatile boolean stop = false;

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        cg.add(e.getChannel());
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        Channel ch = e.getChannel();
        bq.put(ch);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        e.getCause().printStackTrace();
        e.getChannel().close();
    }
    private static void cycleRead() {
        for(Channel ch : bq){
            ChannelBuffer cb = ChannelBuffers.buffer(8) ;
  //          ch.getPipeline().;
        }
    }
    private static void cycleWrite() {
        long ns = System.nanoTime();
        System.out.println("Server send:" + ns);
        for (Channel ch : bq) {
            ChannelBuffer cb = ChannelBuffers.buffer(8);
            cb.writeLong(ns);
            if (stop) {
                ChannelFuture f = ch.write(cb);
                f.addListener(new ChannelFutureListener() {
                    public void operationComplete(ChannelFuture future) {
                        Channel aCh = future.getChannel();
                        System.out.println("channel " + aCh + " closed");
                        aCh.close();
                        bq.remove(aCh);
                    }
                });
            } else {
                ch.write(cb);
            }
        }
    }

    @Override
    public void run() {
        System.out.println("Server Start!");
        ChannelFactory factory =
                new NioServerSocketChannelFactory(
                        Executors.newCachedThreadPool(),
                        Executors.newCachedThreadPool());
        ServerBootstrap bootstrap = new ServerBootstrap
                (factory);
        bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            public ChannelPipeline getPipeline() {
                return Channels.pipeline(new NettyServer());
            }
        });
        bootstrap.setOption("child.tcpNoDelay", true);
        bootstrap.setOption("child.keepAlive", true);
        bootstrap.bind(new InetSocketAddress(8080));
        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (i == 9) {
                stop = true;
            }
//            cycleRead();
            cycleWrite();
        }
        cg.close().awaitUninterruptibly();
        factory.releaseExternalResources();
        System.out.println("Server End!");
    }
}