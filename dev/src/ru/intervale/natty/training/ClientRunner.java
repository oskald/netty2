package ru.intervale.natty.training;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Rasta-Laptop
 * Date: 04.11.12
 * Time: 15:49
 */
public class ClientRunner {

    public static final int CLIENTS_NUMBER = 1;

    public static void main(String[] args) {
        List<Thread> clients = new ArrayList<Thread>();
        for(int i=0; i<CLIENTS_NUMBER; i++){
            clients.add(new NettyClient("Client #" + i));
        }
        Thread server = new Thread(new NettyServer());
        server.setName("Netty server");

        System.out.println("Run server");
        server.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Run clients");
        for(Thread client:clients){
            client.start();
        }


    }
}
