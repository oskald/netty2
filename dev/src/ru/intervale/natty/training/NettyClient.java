package ru.intervale.natty.training;

import java.io.*;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: Rasta-Laptop
 * Date: 04.11.12
 * Time: 14:30
 */
public class NettyClient extends Thread {

    private String name;

    public NettyClient(String name) {
        super(name);
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(name + " Start");

        String str = "param1=10;param2=30;";
        byte[] array = str.getBytes();
        byte[] first = new byte[str.length() / 2];
        byte[] second = new byte[str.length() - (str.length() / 2)];
        System.arraycopy(array, 0, first, 0, str.length() / 2);
        System.arraycopy(array, str.length() / 2, second, 0, str.length()-str.length()/2);

//        System.out.println("send 1:" + new String(first));
//        System.out.println("send 2:" + new String(second));

        try {
            Socket s = new Socket("localhost", 8080);
            OutputStream os = s.getOutputStream();
            os.write(0);
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            os.write(second);
            os.flush();  // вот только сейчас будет установлен коннект
            InputStream is = s.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            while (!s.isClosed()) {
                try{
                    long value = dis.readLong();
                    System.out.println(name + ". Input:" + value);
                } catch (EOFException e){
                    return;
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println(name + " Finish!");
        }

    }

}
